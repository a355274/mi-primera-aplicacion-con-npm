const sumar = require('../index');
const assert = require('assert');

describe("Probar la suma de dos números", ()=>{
    it("Cinco mas cinco es 10", ()=>{
        assert.equal(10, sumar(5,5));
    });
    it("Cinco mas cinco no es 55", ()=>{
        assert.notEqual(55, sumar(5,5));
    });
});