# Mi primera aplicación con NPM
Este proyecto tiene como objetivo proporcionar una introducción a la creación de una aplicación Node.js haciendo uso de diversas dependencias.
Se utilizaron las siguientes dependencias:
- Manejador de log's
- Herramienta de refresco en caliente
- Pruebas unitarias
- Lint

## Autor
Jair Delval Aguirre. 355274
