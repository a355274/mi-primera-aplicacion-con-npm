const log4js = require('log4js');

const logger = log4js.getLogger();
logger.level = "info";


logger.debug("Iniciando la app en modo depuración");
logger.info("La app ha iniciado correctamente");
logger.warn("Falta un archivo en la app");
logger.error("No se pudo acceder al sistema de archivos");
logger.fatal("La App no se pudo ejecutar en el SO");

let varUno=1;

function sumar(x, y){
    return x + y;
}

module.exports = sumar;